**Introduction:**  
Welcome to the GitLab Repo for the Pilot edition of Hack4Good this FS19.  
The repo will be made public and liscenced under your name with an open-source 
liscence after the end of the program, such that other NGOs and the Humanitarian
sector may benefit from your great work! That being said, enjoy the ride and use 
your skills to make this world a better place! 

Your Hack4Good team (:


**Useful Links:**
*  [H4G-Pilot Edition Drive](https://drive.google.com/drive/folders/0AF2FBJA_JreeUk9PVA) (for scheduling calls and asking questions to our NGO partners)
*  [Impact Initiatives website](http://www.impact-initiatives.org/)

**Workshop Dates:**

| Event | Date | Location |
| ------ | ------ | ------ |
| Teamwork time in the SPH (optional) |  24.4.19 (Wed) , 17:00 - 19:00| SPH |
| Workshop 1: Agile workshop for Data Science by external experts | 26.4.19 (Fri), 16:30 - 19:30 | HG E.42 |
| Teamwork time in the SPH (optional) | 29.4.19 (Mon), 17:00 - 19:00 | SPH |
| Workshop 2: Feedback workshop | 8.5.19 (Wed), 17:00 - 19:00 | SPH |
| Workshop 3: Pitching workshop | 15.5.19 (Wed), 17:00 - 19:00 | SPH |
| Final event: Final presentation & Workshop 4: Reflection | 20.5.19 (Mon), 17:30 - 21:30 | SPH |



**Folder Structure**

We have already created a folder structure that should help you starting right away. It should be seen as a guideline and shall help us us
to easier navigate through your code. All present code is exemplatory and you don't have to use any of it. Feel free to delete the existing notebooks as well as the code in src.

```
├── LICENSE
│
│
├── README.md                <- The top-level README for developers using this project
│
├── environment.yml          <- Python environment
│                               
│
├── data
│   ├── processed            <- The final, canonical data sets for modeling.
│   └── raw                  <- The original, immutable data dump.
│
│
├── misc                     <- miscellaneous
│
│
├── notebooks                <- Jupyter notebooks. Every developper has its own folder for exploratory
│   ├── name                    notebooks. Usually every model has its own notebook where models are
│   │   └── exploration.ipynb   tested and optimized. (The present notebooks can be deleted as they only serve for inspiration purposes)
│   └── model
│       └── model_exploration.ipynb <- different optimized models can be compared here if preferred    
│
│
├── reports                   <- Generated analysis as HTML, PDF, LaTeX, etc.
│   └── figures               <- Generated graphics and figures to be used in reporting
│
│
├── results
│   ├── outputs
│   └── models               <- Trained and serialized models, model predictions, or model summaries
│                               (if present)
│
├── scores                   <- Cross validation scores are saved here. (Automatically generated)
│   └── model_name           <- every model has its own folder. 
│
├── src                      <- Source code of this project. All final code comes here (Notebooks are thought for exploration)
│   ├── __init__.py          <- Makes src a Python module
│   ├── main.py              <- main file, that can be called.
│   │
│   │
│   └── utils                <- Scripts to create exploratory and results oriented visualizations
│       └── exploration.py      / functions to evaluate models
│       └── evaluation.py       There is an exemplary implementation of these function in the sample notebook and they should be seen
                                as a help if you wish to use them. You can completely ignore or delete both files.
```

**How to use a python environment**

The purpose of virtual environments is to ensure that every developper has an identical python installation such that conflicts
due to different versions can be minimalized.

**Instruction**

Open a console and move to the folder where your environment file is stored.

* create a python env based on a list of packages from environment.yml

  ```conda env create -f environment.yml -n env_your_proj```

* update a python env based on a list of packages from environment.yml

  ```conda env update -f environment.yml -n env__your_proj```

* activate the env  

  ```activate env_your_proj```
  
* in case of an issue clean all the cache in conda

   ```conda clean -a -y```

* delete the env to recreate it when too many changes are done  

  ```conda env remove -n env_your_proj```