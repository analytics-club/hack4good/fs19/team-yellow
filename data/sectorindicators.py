import pandas as pd
import numpy as np


def pick_cols_with(df, prefix):
    return df.columns[pd.Series(df.columns).str.startswith(prefix)]


def to_cats_with_rename(data, cat_names, cols):
    for c in cols:
        data[c] = data[c].astype("category").cat.rename_categories(cat_names)


# Merge individual HH and clean data with ANY
# Clean data "_uuid" = HH member "UNIQUE RECORD/HOUSEHOLD IDENTIFIER"
def merge_hh_clean_any(data, datahh, series):
    return pd.concat([datahh["UNIQUE RECORD/HOUSEHOLD IDENTIFIER"], series], axis=1)\
             .groupby("UNIQUE RECORD/HOUSEHOLD IDENTIFIER")\
             .any()\
             .reindex(data["_uuid"], fill_value=False)\
             .set_index(data.index)


def load_clean_data(path):
    data = pd.read_excel(path, sheet_name="clean_hh_data")
    # Pick only rows with Consent = Yes (this needs to be taken into account when analyzing data)
    data.drop(data.Consent[data.Consent != "Yes, agrees to interview"].index, inplace=True)
    # Remove columns
    cols_to_remove = [ "Consent", "start", "end" ]
    data.drop(cols_to_remove, axis=1, inplace=True)

    cols_lang = data.columns[data.columns.str.match('Language')]
    cols_special = [ "_id", "_uuid", "_parent_index" ]

    cols_weights = [ "matching_weights", "WEIGHT_IND_LGA", "WEIGHT_HH_DOMAIN", "WEIGHT_HH_STATE", "HH_ALL" ]

    # Change column types
    # Note, that the list of categorical columns is not complete,
    # only ones used for weights calculation are included.
    
    cols_categorical = [ 
        "State", "LGA", "Ward", "Village", "CLUSTER NAME", "cluster_id", "Domain", 
        "Population Group", "Population Group - Status Check", "Respondent Sex", 
        "Head of Household Sex", "Head of Household Marital Status", 
        "How does your current level of income compare to the previous 3 months?",
        "How far is the closest health facility to you?",
        "Has anyone in your household experienced movement restrictions in your area in t",
    ]

    for c in cols_categorical:
        data[c] = data[c].astype("category")

    cat_yndk = {
        1: "Yes",
        2: "No",
        98: "No reponse",
        99: "Don't know"
    }
    cols_yndk = [ 
        "Head of Household - Y/N", "Did you have enough water in the last 30 days to meet your household needs ?", 
        "Is the shelter damaged?", "Are you at risk of being evicted or forced to leave this shelter within the next", 
        "Have you had physical access to a market in the last two weeks?",
        "Is your household in debt (of money?)",
        "Has anyone in your household been sick in the past two weeks?",
        "Has anyone in your household experienced any security incidents in the last thre.1",
        "Is there any member of your household who is missing or being detained?",
    ]
    to_cats_with_rename(data, cat_yndk, cols_yndk)

    cat_yns = {
        1: "Yes, always",
        2: "Yes, sometimes",
        3: "No",
        98: "No response",
        99: "Don't know"
    }
    cols_yns = [ "Do you have to pay to access water from your main source?" ]
    to_cats_with_rename(data, cat_yns, cols_yns)

    return data


def load_hh_data(path):
    return pd.read_excel(path, sheet_name="ind_hh_member_data")


# Indicators Calculations are based on the following documents:
# - Annex_Indicator_PiN.docx
# - Annex_Indicator_PiN.pdf
# - reach_nga_report_2018_multi_sector_needs_assessment_march_2019_0.pdf, Annex 6: Indicators and Thresholds for Sectoral Index Indicators ([link](http://bit.ly/2C5O7Cs)).

# Note that at the moment "Don't know" and "No response" answers are excluded.


###############################
#####     WASH SECTOR     #####
###############################

def wash_no_water_access(data):
    w = 2.0
    series = data[["WATER SOURCE - Borehole / tubewell",
                   "WATER SOURCE - Public tap / standpipe",
                   "WATER SOURCE - Piped into dwelling or plot",
                   "WATER SOURCE - Handpump",
                   "WATER SOURCE - Protected well",
                   "WATER SOURCE - Protected spring" ]] \
            .eq("No").all(1, skipna=True) * w
    series.name = "WASH_No_Water_Access"
    return series

def wash_below_15lpd(data):
    w = 3.0
    series = (data["HOUSEHOLD WATER CONSUMPTOPN (liters per person per day)"] < 15) * w
    series.name = "WASH_Below_15lpd"
    return series

def wash_no_latrine_access(data):
    w = 2.0
    # Note that the response for "Other" doesn't contain any special answers
    series = (~data["Do the members of your household have access to a functioning latrine?"] \
                .isin(["Yes, have latrine access", "No response", "Other"])) * w
    series.name = "WASH_No_Latrine_Access"
    return series

def wash_above_30min(data):
    w = 2.0
    # TODO: check travel time etc
    series = data["How long does it take to collect water from your main water source, including tr"] \
                .isin(["From 30 minutes up to 1 hour", "From 1 hour up to 2 hours", "Greater than 2 hours"]) * w
    series.name = "WASH_Above_30min"
    return series

def wash_no_soap(data):
    w = 1.0
    series = (data["Do you have soap in your household for handwashing?"] == "No") * w
    series.name = "WASH_No_Soap"
    return series


######################################
#####     SHELTER/NFI SECTOR     #####
######################################

def shelter_inadequate(data):
    w = 2.0
    # "Other" response column is empty
    series = data["What is the type of shelter?"] \
                .isin(["Tent",
                       "Makeshift (thatch house with collected materials)",
                       "Collective shelter (mosque, school or other public building)",
                       "No shelter / sleeps in the open space"]) * w
    series.name = "SHELTER_Inadequate"
    return series

def shelter_damaged(data):
    w = 2.0
    # data["Is the shelter damaged?"] == "Yes" includes option "No or very minimal damage"
    series = data["What is the severity of the damage to the shelter overall?"] \
                .isin([ "Partially damaged", "Completely destroyed (100%)" ]) * w
    series.name = "SHELTER_Damaged"
    return series

def shelter_eviction_risk(data):
    w = 2.0
    series = (data["Are you at risk of being evicted or forced to leave this shelter within the next"] == "Yes") * w
    series.name = "SHELTER_Eviction_Risk"
    return series

def shelter_no_basic_items(data):
    w = 2.0
    # Basic NFI Kit in the document has criteria "<7 of 13" but mentions only 12 items
    # We exclude School supplies ("NFIS OWNED - School bags", "NFIS OWNED - School notebooks", "NFIS OWNED - School textbooks")
    # because they're considered in EDUCATION sector
    # This leaves 19 columns, so we check <10 or None (?)
    series = \
        data[["NFIS OWNED - Blankets",
              "NFIS OWNED - Sleeping mat",
              "NFIS OWNED - Mosquito net",
              "NFIS OWNED - Jerry cans",
              "NFIS OWNED - Laundry detercapture gent / bars",
              "NFIS OWNED - Bath soap",
              "NFIS OWNED - Reusable sanitary pad",
              "NFIS OWNED - Solar lamp",
              "NFIS OWNED - Foldable mattress",
              "NFIS OWNED - Kettle",
              "NFIS OWNED - 10L Basin",
              "NFIS OWNED - Rope",
              "NFIS OWNED - Cooking pots",
              "NFIS OWNED - Stainless trays",
              "NFIS OWNED - Stainless cups",
              "NFIS OWNED - Serving spoons",
              "NFIS OWNED - Kitchen knife",
              "NFIS OWNED - 10L Bucket",
              "NFIS OWNED - Aquatabs"]] \
        .eq("Yes").sum(axis=1) < 10
    series = (series | (data["NFIS OWNED - None"] == "Yes")).astype("float")
    series.name = "SHELTER_No_Basic_Items"
    return series

def shelter_many_families(data):
    w = 2.0
    # Note that there're 725 answers = 0 (?)
    series = (data["How many households, including yours, share your current accomodation? "] > 1) * w
    series.name = "SHELTER_Many_Families"
    return series


########################################
#####     FOOD SECURITY SECTOR     #####
########################################

# Food Consumption Score calculated using methodology from the 2018 report, see
# https://documents.wfp.org/stellent/groups/public/documents/manual_guide_proced/wfp197216.pdf?_ga=2.42910636.848875997.1543498323-2145050901.1543498323
def foodsec_cons_score(data):
    # Multiply the value obtained for each food group by its weight 
    # Sum the weighed food group scores, split into categories
    # Poor (0 - 21), Borderline (21.5 - 35), Acceptable (> 35)
    # FCS could potentially be used as a standalone feature
    # max w = 3
    w_poor       = 3.0
    w_borderline = 2.0
    
    series = 2.0 * data["FCS - Cereals"] + \
             3.0 * data["FCS - Pulses"] + \
             1.0 * data["FCS - Vegetables"] + \
             1.0 * data["FCS - Fruits"] + \
             4.0 * data["FCS - Meats"] + \
             4.0 * data["FCS - Dairy"] + \
             0.5 * data["FCS - Oils"] + \
             0.5 * data["FCS - Sweets"] + \
             0.0 * data["FCS - Spices"]

    criteria  = [series.lt(21.5), series.between(21.5, 35), series.gt(35)]
    values    = [w_poor, w_borderline, 0.0]
    series[:] = np.select(criteria, values, 0)

    series.name = "FOODSEC_FCS"
    return series


# Reduced Coping Strategy Index
def foodsec_rcsi(data):
    w = 3.0
    # TODO: check methodology
    series = 1.0 * data["RCSI - Less preferred foods"] + \
             2.0 * data["RCSI - Borrow food or food on credit"] + \
             1.0 * data["RCSI - Limit portion size at meals"] + \
             3.0 * data["RCSI - Restrict consumption by adults in order for small children to eat"] + \
             1.0 * data["RCSI - Reduce meals eaten in a day"]
    series = (series >= 10.0) * w
    series.name = "FOODSEC_RCSI"
    return series

def foodsec_safe(data):
    w = 2.0
    # TODO: what about option None in Q. about primary means?
    series = \
        0.33 * data["What is the most commonly used fuel type for COOKING in your household?"] \
                .isin(["Firewood", "Agricultural waste / crop residue", "Animal dung"]) \
      + 0.33 * data["What is the most commonly used fuel type for LIGHTING in your household?"] \
                .isin(["Firewood", "None"]) \
      + 0.33 * data["What is the most commonly used method of cooking in your household?"] \
                .isin(["Three-stone fire"]) \
      + 0.33 * data["What is your primary means of obtaining firewood or whichever fuel source you pr"] \
                .isin(["Collect directly from outside the community",
                       "From NGO aid  / assistance",
                       "Trade goods or items for fuel"]) \
      + 2.00 * data[["COPING FUEL - Selling food/rations to buy fuel",
                     "COPING FUEL - Begging for fuel"]].eq("Yes").any(axis=1) \
      + 0.33 * data[["COPING FUEL - Use less preferred fuel source (animal dung, etc.)",
                     "COPING FUEL - Borrowing fuel/firewood",
                     "COPING FUEL - Collect firewood from the bush",
                     "COPING FUEL - Send children to collect firewood"]].eq("Yes").any(axis=1)
    # The answers might potentially give a score > 2, so we cap it at 2 for now
    series = np.maximum(series, w)
    series.name = "FOODSEC_SAFE"
    return series

def foodsec_market(data):
    # max w = 2.0
    # Should limited income be included as well? What about other barriers (see data["BARRIER FOOD - Other - text"])?
    series = \
        1.0 * (data["Have you had physical access to a market in the last two weeks?"] == "No") \
      + 1.0 * (data[["BARRIER FOOD - Market is too far away",
                     "BARRIER FOOD - Transportation to market is too expensive",
                     "BARRIER FOOD - Food prices are unusually high",
                     "BARRIER FOOD - Food not available in the market",
                     "BARRIER FOOD - Cannot access market due to PERCEIVED INSECURITY",
                     "BARRIER FOOD - Cannot access market due to MOVEMENT RESTRICTIONS by armed groups"]] \
                   .eq("Yes").any(axis=1))
    series.name = "FOODSEC_Market"
    return series

def foodsec_agriculture(data):
    # max w = 2.0
    series = \
        0.50 * (data["Was your household able to plant and harvest crops during the last dry season?"] \
                .isin(["Didn't plant or harvest", "Planted but did not harvest anything"]).astype("float")) \
      + 0.50 * (data["Are you planning on planting and harvesting for this rainy season (2018)?"] \
                .isin(["No, will not plant or harvest this rainy season"]).astype("float")) \
      + 0.50 * (data["Were you able to access land?"] == "No, did not access any land") \
      + 0.25 * (data["Were you able to access land?"] == "Yes, but did not access amount of land needed") \
      + 0.50 * (data["Were you able to access water?"] == "No, did not access any water") \
      + 0.25 * (data["Were you able to access water?"] == "Yes, but did not access amount of water needed")
    series.name = "FOODSEC_Agriculture"
    return series


######################################
#####     LIVELIHOODS SECTOR     #####
######################################

def live_incomedec(data):
    w = 2.0
    series = (data["How does your current level of income compare to the previous 3 months?"] == "Decrease") * w
    series.name = "LIVE_Income_Decrease"
    return series

def live_debt(data):
    w = 2.0
    series = (data["Is your household in debt (of money?)"] == "Yes") * w
    series.name = "LIVE_Debt"
    return series


# Stress strategies:    sell HH assets/goods, spend savings, sell (non-productive) animals, send HH members to eat elsewhere, purchase food on credit, or borrow money
# Crisis strategies:    sell productive assets, withdraw children from school, reduce expenses on basic services (education, health), harvest immature crops, consume seed stocks to be saved for next year, decrease expenditure on agricultural/animal-based activities and care
# Emergency strategies: sell house or land, beg for money, engage in illegal/dangerous income activities, sell last reproductive animals, or have the entire HH migrate/displace
def live_coping(data):
    # There's another set of columns which seem to be a duplication of Qs: COPING LIVELIHOD Exhausted
    w = 3.0
    series = data[["COPING LIVELIHOOD - Spend savings", 
                   "COPING LIVELIHOOD - Sell productive assets or means of transportation (sewing ma",
                   "COPING LIVELIHOOD - Reduce expenditure on other services like health and educati",
                   "COPING LIVELIHOOD - Harvest immature crops (green maize, etc.)",
                   "COPING LIVELIHOOD - Consume seed stocks that were to be saved for the next seaso",
                   "COPING LIVELIHOOD - Decrease expenditure on fertilizer, pesticide, animal feed, ",
                   "COPING LIVELIHOOD - Sell land or property",
                   "COPING LIVELIHOOD - Beg for money",
                   "COPING LIVELIHOOD - Engage in dangerous or illegal work/activity (theft, illegal",
                   "COPING LIVELIHOOD - Sell last female (productive) animals"]]\
            .eq("Yes").any(axis=1) * w
    series.name = "LIVE_Coping"
    return series


def live_cash(data):
    w = 3.0
    series = (data["How do you get cash?"] == "No access to cash") * w
    series.name = "LIVE_Cash"
    return series


#################################
#####     HEALTH SECTOR     #####
#################################

def health_barrier(data):
    w = 2.0
    series = (data["BARRIER HEALTH - No barrier"] == "No") * w
    series.name = "HEALTH_Barrier"
    return series

def health_childnovaccines(data, datahh_vaccines):
    # All answers are NaN for:
    # "Has  received any PENTA vaccine shots?",
    # "Has  received any polio (OPV) vaccine?"
    w = 2.0
    series = datahh_vaccines["Has  received any measles vaccine?"] == "No"
    series.name = "HEALTH_Child_No_Vaccines"
    # TODO: disagrees with clean summary, needs to be reviewed
    return merge_hh_clean_any(data, datahh_vaccines, series) * w

def health_illness2w(data):
    w = 2.0
    series = (data["Has anyone in your household been sick in the past two weeks?"] == "Yes") * w
    series.name = "HEALTH_Illness_2w"
    return series

def health_distance2km(data):
    w = 2.0
    series = data["How far is the closest health facility to you?"]\
                .isin(["Within 2-5km", "More than 5km"]) * w
    series.name = "HEALTH_Distance_2km"
    return series

def health_delivery(data):
    w = 2.0
    series = data["Who helped attend this birth?"]\
                .isin(["Traditional birth attendant",
                       "Other health care worker (health volunteer, CHEW)",
                       "Other women in the community",
                       "No support",
                       "Family members"]) * w
    series.name = "HEALTH_Delivery"
    return series


####################################
#####     EDUCATION SECTOR     #####
####################################

def edu_notattending(data, datahh_edu):
    w = 3.0
    
    formal = \
        datahh_edu["What is the current FORMAL school attendance status of ?"]\
        .isin(["Did not attend any formal school this year",
               "Dropped out this year",
               "Never attended any formal school"])

    informal = \
        datahh_edu["What is the current INFORMAL school attendance status of ?"]\
        .isin(["Did not attend non-formal education this year",
               "Dropped out of non-formal education this year", 
               "Never attended any non-formal education"])
    series = (formal & informal)

    series.name = "EDU_Not_Attending"
    return merge_hh_clean_any(data, datahh_edu, series) * w

def edu_neverattendedformal(data, datahh_edu):
    w = 3.0
    series = datahh_edu["What is the current FORMAL school attendance status of ?"] == "Never attended any formal school"
    series.name = "EDU_Never_Attended_Formal"
    return merge_hh_clean_any(data, datahh_edu, series) * w

def edu_barrier(data):
    w = 2.0
    # TODO: is it only access or any barrier?
    series = (data["BARRIER EDUCATION - No barriers"] == "No") * w
    series.name = "EDU_Barrier"
    return series

def edu_nosupplies(data):
    w = 2.0
    series = data[["NFIS OWNED - School bags", "NFIS OWNED - School notebooks", "NFIS OWNED - School textbooks"]] \
                 .eq("No").all(axis=1) * w
    series.name = "EDU_No_Supplies"
    return series


####################################
#####     NUTRITION SECTOR     #####
####################################

# SAM, MAM, OEDEMA
def nutrition(data):
    w = 10.0
    series = (data[["NUMBER CHILDREN with SAM (MUAC <=115)", 
                    "NUMBER CHILDREN with MAM (115mm >= MUAC <125mm)",
                    "NUMBER CHILDREN with OEDEMA",
                    "NUMBER CHILDRNE with SAM in OTP"]] > 0) \
             .any(axis=1) * w
    series.name = "NUTRITION"
    return series


#####################################
#####     PROTECTION SECTOR     #####
#####################################

def protection_explosives(data):
    w = 2.0
    wards_wlandmines = data["Ward"][data["SECURITY INCIDENT TYPE - Presence of landmines / UXOs"] == "Yes"].unique()
    series = data["Ward"].isin(wards_wlandmines) * w
    series.name = "PROTECTION_Explosives"
    return series

def protection_incident3m(data):
    w = 2.0
    # The other question "Has anyone in your household experienced any security incidents in the last thre"
    # has all answers = 0
    series = \
        (data["Has anyone in your household experienced any security incidents in the last thre.1"] == "Yes") * w
    series.name = "PROTECTION_Incident_3m"
    return series

def protection_nodocs(data):
    w = 2.0
    series = (data["Of the adults in the household, how many have some form of legal documentation?"] == 0) * w
    series.name = "PROTECTION_No_Docs"
    return series

def protection_movement(data):
    w = 2.0
    series = data["Has anyone in your household experienced movement restrictions in your area in t"]\
            .isin(["Yes, but only during the evening and nighttime",
                   "Yes, from 5-10km outside of the camp or community",
                   "Yes, but only if there are multiple household members",
                   "Yes, complete movement restrictions"]) * w
    series.name = "PROTECTION_Movement"
    return series
    
def protection_missingmembers(data):
    w = 2.0
    series = (data["Is there any member of your household who is missing or being detained?"] == "Yes") * w
    series.name = "PROTECTION_Missing_Members"
    return series




##########################################################
#####     LOAD CLEAN DATASET AND SAVE WEIGHTS TO CSV #####
##########################################################

def sector_weights(path_data, path_res):
    data   = load_clean_data(path_data)
    datahh = load_hh_data(path_data)
    
    # HH with information about school attendance (age is < 18y for all)
    datahh_edu = datahh.loc[~datahh["What is the current INFORMAL school attendance status of ?"].isna(), :]
    # HH with information about vaccines (age is < 10y for all)
    datahh_vaccines = datahh.loc[~datahh["Has  received any measles vaccine?"].isna(), :]
    
    
    wdata = pd.concat([\
        # WARD
        data["Ward"],
        # WASH 
        wash_no_water_access(data),
        wash_below_15lpd(data),
        wash_no_latrine_access(data),
        wash_above_30min(data),
        wash_no_soap(data),
        # SHELTER
        shelter_inadequate(data),
        shelter_damaged(data),
        shelter_eviction_risk(data),
        shelter_no_basic_items(data),
        shelter_many_families(data),
        # FOOD SECURITY
        foodsec_cons_score(data),
        foodsec_rcsi(data),
        foodsec_safe(data),
        foodsec_market(data),
        foodsec_agriculture(data),
        # LIVELIHOODS
        live_incomedec(data),
        live_debt(data),
        live_coping(data),
        live_cash(data),
        # EDUCATION
        edu_notattending(data, datahh_edu),
        edu_neverattendedformal(data, datahh_edu),
        edu_barrier(data),
        edu_nosupplies(data),
        # HEALTH
        health_barrier(data),
        health_childnovaccines(data, datahh_vaccines),
        health_illness2w(data),
        health_distance2km(data),
        health_delivery(data),
        # NUTRITION
        nutrition(data),
        # PROTECTION
        protection_explosives(data),
        protection_incident3m(data),
        protection_nodocs(data),
        protection_movement(data),
        protection_missingmembers(data)
    ], axis=1)
    
    # Subtotals for sectors
    sectors = [ "WASH", "SHELTER", "FOODSEC", "LIVE", "EDU", "HEALTH", "NUTRITION", "PROTECTION" ]
    for sector in sectors:
        cols = pick_cols_with(wdata, sector)
        # Make sure max sector weight is 10 (might not hold for FOODSEC)
        wdata["WEIGHT_" + sector] = np.minimum(wdata[cols].sum(axis=1), 10.0)
    
    # Sum of weights across all sectors 
    wdata["INDICATOR"] = wdata[pick_cols_with(wdata, "WEIGHT_")].sum(axis=1)
    # Number of sectors in need
    wdata["N_SECTORS"] = wdata[pick_cols_with(wdata, "WEIGHT_")].gt(0).sum(axis=1)
    wdata.to_csv(path_res)


if __name__ == "__main__":
    path_data = "../reach_nga_msna_clean_dataset_final.xlsx"
    path_res  = "../weights.csv"
    sector_weights(path_data, path_res)
