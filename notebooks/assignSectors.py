#!/usr/bin/env python3
# -*- coding: utf-8 -*-


"""
This code assigns sectors to all the features. We create a list called 'assigned_sectors'.
"""

##imports
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import re
import string as str

##load data
allDomainName = np.load("/data/allDomainName.npy")
allDomainContents = np.load("/data/allDomainContents.npy")
allFeatureName = np.load("/data/allFeatureName.npy")
allDefault = np.load("/data/allDefault.npy")

##typos
allFeatureName[74] = 'Vulnerability - Other - Text'
#allFeatureName = np.delete(allFeatureName, 658) same as 655

##initialize
assigned_sectors = ["other" for x in range(len(allFeatureName))]
sectors = {'language': ['language'], 
           'vulnerability': ['vulnerability'], 
           'water': ['water', 'latrine', 'trash', 'garbage', 'wash'],        
           'health': ['illness', 'health', 'treatment', 'vaccine', 'sick'],
           'food': ['food', 'fcs', 'rcsi'],
           'energy': ['fuel', 'firewood', 'cooking'],
           'livelihood': ['livelihood', 'expenses', 'income', 'cash', 'harvest', 'land'],
           'shelter': ['shelter', 'rent'],
           'non-food items': ['nfis'], 
           'education': ['education'], 
           'security': ['security'], 
           'documentation': ['documentation', 'detain', 'missing'], 
           'aid': ['aid', 'information'], 
           'demographic': ['sex', 'location', 'area of origin', 'aoo', 'push', 'pull', 'age']}

##assign sectors
for column, feature in enumerate(allFeatureName):
    potential_sectors = ['other']
    potential_sectors_prio = [100]
    for sector in sectors.keys():
        for key_word in sectors[sector]:
            search_obj = re.search(key_word, feature.lower())
            if search_obj is not None: #if keyword for one sector is found in the feature name
                potential_sectors.append(sector)
                potential_sectors_prio.append(search_obj.span()[0])
    most_likely_sector = [x for _,x in sorted(zip(potential_sectors_prio, potential_sectors))][0] #prioritize keywords in the beginning of the feature name
    assigned_sectors[column] = most_likely_sector
                   
            
##manual corrections
assigned_sectors[2:14] = ['demographic'] * 12
assigned_sectors[18] = ['demographic']
assigned_sectors[54:63] = ['demographic'] * 9
assigned_sectors[68] = ['vulnerability']
assigned_sectors[80] = ['demographic']
assigned_sectors[100:102] = ['demographic'] * 2
assigned_sectors[269] = ['water']
assigned_sectors[333:354] = ['health'] * 21
assigned_sectors[395] = ['food']
assigned_sectors[432] = ['energy']
assigned_sectors[456] = ['energy']
assigned_sectors[542:544] = ['livelihood'] * 2
assigned_sectors[546] = ['livelihood'] 
assigned_sectors[553:556] = ['livelihood'] * 3
assigned_sectors[576] = ['shelter']
assigned_sectors[579:581] = ['shelter'] * 2
assigned_sectors[595] = ['shelter']
assigned_sectors[621] = ['shelter']
assigned_sectors[622:628] = ['demographic'] * 6
assigned_sectors[653] = ['education']
assigned_sectors[655] = ['security']
assigned_sectors[658] = ['security']
assigned_sectors[688] = ['documentation']
assigned_sectors[699:703] = ['documentation'] * 4
assigned_sectors[719] = ['aid']
assigned_sectors[758] = ['aid']
assigned_sectors[760] = ['aid']
assigned_sectors[778] = ['aid']
assigned_sectors[903:907] = ['aid'] * 4
assigned_sectors[907:914] = ['demographic'] * 7
assigned_sectors[916] = ['other']

assigned_sectors_array = np.array(assigned_sectors, dtype = object)
stacked_array = np.hstack((0, assigned_sectors_array))
np.save('/home/belinda/ownCloud/Documents/studium/statistics/hack4Good/team-1/assigned_sectors.npy', stacked_array)










