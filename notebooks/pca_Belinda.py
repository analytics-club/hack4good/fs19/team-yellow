#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May 17 09:10:15 2019

@author: belinda
"""

##imports
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt 
from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler

##read in data
work_dir = "/home/belinda/ownCloud/Documents/studium/statistics/hack4Good/team-1/data/"
ysDefault = np.load(work_dir + "ysDefault.npy")
ysFeatureName = np.load(work_dir + "ysFeatureName.npy")
y_df = pd.DataFrame(ysDefault)
y_df.columns = ysFeatureName
binDefault = np.load(work_dir + "/binDefault.npy")
binFeatureName = np.load(work_dir + "binFeatureName.npy")
data_df = pd.DataFrame(binDefault)
data_df.columns = binFeatureName


##compute overall PIN score
overall_PIN = y_df.iloc[:, 0:6].sum(axis = 1)


##perform PCA on data
scaler = StandardScaler().fit(data_df)
data_pre = scaler.transform(data_df)
pca = PCA(n_components = 5, copy=True, whiten=False)
pca.fit(data_pre)
expl_var = pca.explained_variance_ratio_ 
pcs = pca.components_
scores = pca.fit_transform(data_pre)
scores_df = pd.DataFrame(scores)

##plot pcs
pd.plotting.scatter_matrix(scores_df, c = y_df['WEIGHT_FOODSEC'])
pd.plotting.scatter_matrix(scores_df, c = y_df['WEIGHT_LIVE'])
pd.plotting.scatter_matrix(scores_df, c = overall_PIN)